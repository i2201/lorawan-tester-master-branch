var indexSectionsWithContent =
{
  0: "abefglmrs~",
  1: "be",
  2: "be",
  3: "aefgmr~",
  4: "m",
  5: "bs",
  6: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Pages"
};

