/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "LoraWan Tester", "index.html", [
    [ "LoRaWAN Test Device", "index.html", [
      [ "Overview", "index.html#autotoc_md1", null ],
      [ "Use case", "index.html#autotoc_md2", null ],
      [ "Principles", "index.html#autotoc_md3", null ],
      [ "Hardware", "index.html#autotoc_md4", null ],
      [ "Installation Guide", "index.html#autotoc_md5", [
        [ "<em><strong>Step 1 - Preparations</strong></em>", "index.html#autotoc_md6", null ],
        [ "<em><strong>Step 2 - Installing the Firmware</strong></em>", "index.html#autotoc_md7", null ],
        [ "<em><strong>Step 3 - Adding the device to Helium</strong></em>", "index.html#autotoc_md8", null ],
        [ "<em><strong>Step 4 - Adding the Helium device keys to the code</strong></em>", "index.html#autotoc_md9", null ],
        [ "<em><strong>Step 5 - Making small changes to the libraries</strong></em>", "index.html#autotoc_md10", null ],
        [ "<em><strong>Step 6 - Setting up the Decoder Function</strong></em>", "index.html#autotoc_md11", null ],
        [ "<em><strong>Step 7 - Adding integrations</strong></em>", "index.html#autotoc_md12", null ],
        [ "<em><strong>Step 8 - Setting up the Flow</strong></em>", "index.html#autotoc_md13", null ]
      ] ],
      [ "User Guide", "index.html#autotoc_md14", [
        [ "<em><strong>Step 1 - Choose the function</strong></em>", "index.html#autotoc_md15", null ],
        [ "<em><strong>Step 2 - Run the function</strong></em>", "index.html#autotoc_md16", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_beeldscherm_8cpp.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';