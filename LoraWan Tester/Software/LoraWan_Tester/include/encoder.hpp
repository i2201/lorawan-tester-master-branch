/// \file encoder.hpp
/// \brief Encoder class lorawan tester
/// \date 28-3-2022
/// \author Marijn Aarts 
/// \version 1.0

#ifndef ENCODER_HPP
#define ENCODER_HPP

//#include <iostream>
#include <GPS_Air530Z.h>

/// \brief Number of bytes in a  message
#define SEND_BUFFER_LENGTH 8

/// \brief Flag to indicate whether battery data is being sent
#define BATTERY_DATA_PRESENT_FLAG 0x08

/// \class encoder
/// \brief All functions to add data to the encoded sent buffer
/// \details This class adds all GPS data to a sent buffer and optionally the battery data. The buffer will be SEND_BUFFER_LENGTH - 1 bytes long if the battery data is not added. 
class encoder{
public:
    /// \brief Default constructor, reset() is called to initialize the buffer with zeros
    encoder();

    /// \brief Default deconstructor
    ~encoder() {}

    /// \brief Get a pointer to the send buffer
    /// \param buffer_size pointer to an int where the size of the buffer will be stored
    /// \return pointer to send buffer
    char *get_send_bufferptr(int *buffer_size);

    /// \brief Adds GPS data to the send buffer
    /// \param gps_input The GPS data to be added
    void add_gps_data(Air530ZClass gps_input);

    /// \brief Adds battery data to the send buffer
    /// \param battery_level The battery level data to be added
    void add_battery_data(const float battery_level);

    /// \brief Resets send buffer to zero
    void reset(void);

private:

    char send_buffer_battery[SEND_BUFFER_LENGTH];           ///< Send buffer where the battery data is included
    char send_buffer_no_battery[SEND_BUFFER_LENGTH - 1];    ///< Send buffer where the battery data is excluded
    
    /// \brief Adds longitude data to the send buffer
    /// \param longitude_input The longitude data to be added
    void add_longitude(const double longitude_input);

    /// \brief Adds latitude data to the send buffer
    /// \param latitude_input The latitude data to be added
    void add_latitude(const double latitude_input);

    /// \brief Adds accuracy data to the send buffer
    /// \param accuracy_input The accuracy data to be added
    void add_accuracy(const double accuracy_input);

    /// \brief Adds altitude data to the send buffer
    /// \param altitude_input The altitude data to be added    
    void add_altitude(const double altitude_input);

};

#endif // ENCODER_HPP
