#ifndef BEELDSCHERM_HPP
#define BEELDSCHERM_HPP

/// \file Beeldscherm.hpp
/// \brief Display class lorawan tester
/// \date 31-3-2022
/// \author Jacob Willemsen
/// \version 1.1

#include <Wire.h>
#include "Arduino.h"
#include "HT_SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`
#include "HT_DisplayUi.h"
#include "images.h"

/// \class Beeldscherm
/// \brief All functions to control the display on the board
/// \details This class is made to control the HT_SSD1306 display located on the board. the function will be called to show a particular frame correlating to a function 
class Beeldscherm{
public:
  /// \brief Sets the pins and initiates the ui en display functions
  void GUIInit();

  /// \brief Draws a frame for the boot screen with the preset parameters of the drawFrame1 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame1();
  /// \brief Draws a frame to display the ongoing joining function with the preset parameters of the drawFrame2 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame2();
  /// \brief Draws a frame to display the joining function finished with the preset parameters of the drawFrame3 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame3();
  /// \brief Draws a frame to display the ongoing sending function with the preset parameters of the drawFrame4 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame4();
  /// \brief Draws a frame to display the sending function finished and waits anew with the preset parameters of the drawFrame5 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame5();
  /// \brief Draws a frame for the Single_capture_Sending function with the preset parameters of the drawFrame6 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame6();
  /// \brief Draws a frame for the Single_capture_Done function with the preset parameters of the drawFrame7 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame7();
  /// \brief Draws a frame for the Multi capture menu pointing at value 1 with the preset parameters of the drawFrame8 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame8();
  /// \brief Draws a frame for the Multi capture menu pointing at value 2 with the preset parameters of the drawFrame9 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame9();
  /// \brief Draws a frame for the Multi capture menu pointing at value 3 with the preset parameters of the drawFrame10 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame10();
  /// \brief Draws a frame for the Multi capture menu pointing at value 4 with the preset parameters of the drawFrame11 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame11();
  /// \brief Draws a frame for the Multi_capture_Sending function with the preset parameters of the drawFrame12 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame12();
  /// \brief Draws a frame for the Multi_capture_Done function with the preset parameters of the drawFrame13 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame13();
  /// \brief Draws a frame for the GPS function with the preset parameters of the drawFrame14 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame14();
  /// \brief Draws a frame for the Parameters function with the preset parameters of the drawFrame15 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame15();
  /// \brief Draws a frame for an Error promt with the preset parameters of the drawFrame16 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame16();
  /// \brief Draws a frame for the LoRaWan logo with the preset parameters of the drawFrame17 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame17();
  /// \brief Draws a frame for the main menu pointing at the single capture function with the preset parameters of the drawFrame18 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame18();
  /// \brief Draws a frame for the main menu pointing at the multi capture function with the preset parameters of the drawFrame19 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame19();
  /// \brief Draws a frame for the main menu pointing at the GPS function with the preset parameters of the drawFrame20 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame20();
  /// \brief Draws a frame for the main menu pointing at the parameters function with the preset parameters of the drawFrame21 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame21();
  /// \brief Draws a frame for the Single_capture_ACK function with the preset parameters of the drawFrame22 and msOverlay function.
  /// \param ScreenDisplay contains the specifications of the display used
  void frame22();

private:
/// \brief sets the parameters for frame 1 to be drawn when the frame1 function is called
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame1(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for frame 2 to be drawn when the frame2 function is called
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame2(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for frame 3 to be drawn when the frame3 function is called
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame3(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for frame 4 to be drawn when the frame4 function is called
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame4(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for frame 5 to be drawn when the frame5 function is called
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame5(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Single_capture_Sending function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame6(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Single_capture_Done function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame7(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Multi capture menu pointing at value 1
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame8(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Multi capture menu pointing at value 2
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame9(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Multi capture menu pointing at value 3
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame10(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Multi capture menu pointing at value 4
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame11(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Multi_capture_Sending function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame12(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Multi_capture_Done function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame13(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the GPS function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame14(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Parameters function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame15(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of an Error promt
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame16(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the LoRaWan logo
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame17(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the main menu pointing at the single capture function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame18(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the main menu pointing at the multi capture function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame19(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the main menu pointing at the GPS capture function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame20(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the main menu pointing at the Parameter capture function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame21(ScreenDisplay *display, int16_t x, int16_t y);
  /// \brief sets the parameters for the frame of the Single_capture_ACK function
  /// \param ScreenDisplay contains the specifications of the display used
  /// \param x defines the offset to draw from the X axis
  /// \param y defines the offset to draw from the Y axis
  void drawFrame22(ScreenDisplay *display, int16_t x, int16_t y);

  /// \brief sets the parameters for the overlay to be drawn when a frame function is called.
  /// \param ScreenDisplay contains the specifications of the display used
  void msOverlay(ScreenDisplay *display);
};



#endif

