/// \file LoraWan_Tester.ino
/// \brief class lorawan tester
/// \date 2-4-2022
/// \author Marijn Aarts
/// \version 1.0

#include "include/encoder.hpp"
#include "LoRaWan_APP.h"
#include "Arduino.h"
#include "include/Beeldscherm.hpp"

/* OTAA para*/
uint8_t devEui[] = { 0x60, 0x81, 0xF9, 0x4E, 0x46, 0xEF, 0x0C, 0x91 }; //6081F94E46EF0C91 6081F94E46EF0C91
uint8_t appEui[] = { 0x60, 0x81, 0xF9, 0xF8, 0x7B, 0x5F, 0x49, 0x6F }; //6081F9F87B5F496F 6081F9F87B5F496F
uint8_t appKey[] = { 0x48, 0x8F, 0x03, 0xE3, 0xA3, 0xBE, 0x99, 0x70, 0x1F, 0x25, 0xD4, 0x5F, 0x48, 0xAD, 0xE2, 0x57 }; //488F03E3A3BE99701F25D45F48ADE257 488F03E3A3BE99701F25D45F48ADE257

/* ABP para*/
uint8_t nwkSKey[] = { 0x15, 0xb1, 0xd0, 0xef, 0xa4, 0x63, 0xdf, 0xbe, 0x3d, 0x11, 0x18, 0x1e, 0x1e, 0xc7, 0xda,0x85 };
uint8_t appSKey[] = { 0xd7, 0x2c, 0x78, 0x75, 0x8c, 0xdc, 0xca, 0xbf, 0x55, 0xee, 0x4a, 0x77, 0x8d, 0x16, 0xef,0x67 };
uint32_t devAddr =  ( uint32_t )0x007e6ae1;

extern bool isjoined;

/*LoraWan channelsmask, default channels 0-7*/ 
uint16_t userChannelsMask[6]={ 0x00FF,0x0000,0x0000,0x0000,0x0000,0x0000 };

/*LoraWan region, select in arduino IDE tools*/
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;

/*LoraWan Class, Class A and Cla  ss C are supported*/
DeviceClass_t  loraWanClass = LORAWAN_CLASS;

/*the application data transmission duty cycle.  value in [ms].*/
uint32_t appTxDutyCycle = 15000;

/*OTAA or ABP*/
bool overTheAirActivation = LORAWAN_NETMODE;

/*ADR enable*/
bool loraWanAdr = LORAWAN_ADR;

/* set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again */
bool keepNet = LORAWAN_NET_RESERVE;

/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = LORAWAN_UPLINKMODE;

/* Application port */
uint8_t appPort = 2;

uint8_t confirmedNbTrials = 4;

encoder my_encoder;
Beeldscherm my_Beelscherm;

int buffer_size = 0;

Air530ZClass GPS;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  my_Beelscherm.GUIInit();
  GPS.begin();

  uint32_t starttime = millis();
  
  Air530ZClass oldGPS = GPS;

    while( oldGPS.location.lat() == GPS.location.lat())
    {
      while (GPS.available() > 0)
      {
        GPS.encode(GPS.read());
      }
    }


  Serial.println("GPS data received");
  
  my_encoder.add_gps_data(GPS);

}

void loop() {
  // put your main code here, to run repeatedly:

  my_Beelscherm.frame1();
  delay(5000);
  my_Beelscherm.frame2();

  getDevParam();
  printDevParam();
  LoRaWAN.init(loraWanClass,loraWanRegion);
  LoRaWAN.join();

  my_Beelscherm.frame3();

 

  char *array = my_encoder.get_send_bufferptr(&buffer_size);

  appDataSize = buffer_size;
  for(int i =0; i<buffer_size; i++)
  {
      appData[i] = array[i];
  }

  while(!isjoined)
  {
    delay(1000);
    my_Beelscherm.frame16();
  }

  //delay(10000);
  Serial.println("Sending packet");

  LoRaWAN.send();
  Air530ZClass lastGPS = GPS;
  while(1)
  {

    my_Beelscherm.frame4();
    my_encoder.reset();
    delay(10000);
    while(lastGPS.location.lat() == GPS.location.lat())
    {
      while (GPS.available() > 0)
      {
        GPS.encode(GPS.read());
      }
    }

    lastGPS = GPS;
    my_encoder.add_gps_data(GPS);

    char* myArray = my_encoder.get_send_bufferptr(&buffer_size);
    appDataSize = buffer_size;
    for(int i =0; i<buffer_size; i++)
    {
        appData[i] = array[i];
    }
    Serial.println("Sending packet");
    LoRaWAN.send();

    my_Beelscherm.frame5();

  }


}

