/// \file Beeldscherm.cpp
/// \brief Display class lorawan tester
/// \date 31-3-2022
/// \author Jacob Willemsen
/// \version 1.1

#include "include/Beeldscherm.hpp"

/// \brief input for the display. addr , freq , SDA, SCL, resolution , rst.
SSD1306Wire  mydisplay(0x3c, 500000, SDA, SCL, GEOMETRY_128_64, GPIO10); // addr , freq , SDA, SCL, resolution , rst
/// \brief selection of the display for the UI class
DisplayUi ui( &mydisplay );

void Beeldscherm::msOverlay(ScreenDisplay *mydisplay) {

  mydisplay->setTextAlignment(TEXT_ALIGN_RIGHT);
  mydisplay->setFont(ArialMT_Plain_10);
  mydisplay->drawString(128, 0, "Battery life = 100%");
  //display->drawString(150, 0, this.text);
}

void Beeldscherm::drawFrame1(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //boot screen
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 21 + y, "Welcome - LoraWan IoTL");
  mydisplay->drawString(64 + x, 31 + y, "Made by Juul, Marijn, Jens, Quan, Denis, Jacob");
  mydisplay->drawString(64 + x, 41 + y, "Quan, Denis, Jacob");


  mydisplay->display();

}

void Beeldscherm::drawFrame2(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //joining
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 26 + y, "Requestion to join");

  mydisplay->display();

}

void Beeldscherm::drawFrame3(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //joined
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 26 + y, "Joined");

  mydisplay->display();

}

void Beeldscherm::drawFrame4(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //sending packet
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 26 + y, "Sending a single package");


  mydisplay->display();

}

void Beeldscherm::drawFrame5(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //package received waiting for new packet
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 21 + y, "Package send");
  mydisplay->drawString(64 + x, 31 + y, "waiting to send a new one");
  mydisplay->display();


}

void Beeldscherm::drawFrame6(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Single capture function
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 26 + y, "Capturing a single package");

  mydisplay->display();

}

void Beeldscherm::drawFrame7(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Single capture Done
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 21 + y, "Capturing Done");

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 31 + y, "Press A to continue");

  mydisplay->display();

}

void Beeldscherm::drawFrame8(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Multi capture value1
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Capturing time:");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "1 min <--");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 21 + y, "30 min");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "1 hour");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 31 + y, "2 hour");

  mydisplay->display();
}

void Beeldscherm::drawFrame9(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Multi capture value2
  mydisplay->clear();
  msOverlay(mydisplay);
  
  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Capturing time:");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "1 min");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 21 + y, "30 min <--");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "1 hour");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 31 + y, "2 hour");

  mydisplay->display();
}

void Beeldscherm::drawFrame10(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Multi capture value3
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Capturing time:");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "1 min");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 21 + y, "30 min");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "1 hour <--");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 31 + y, "2 hour");

  mydisplay->display();
}

void Beeldscherm::drawFrame11(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Multi capture value4
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Capturing time:");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "1 min");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 21 + y, "30 min");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "1 hour");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(80 + x, 31 + y, "2 hour <--");

  mydisplay->display();
}

void Beeldscherm::drawFrame12(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Multi capture function
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 26 + y, "Capturing multiple packages");

  mydisplay->display();
}

void Beeldscherm::drawFrame13(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Multi capture done
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 21 + y, "Capturing done");

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 31 + y, "Press A to continue");

  mydisplay->display();
}

void Beeldscherm::drawFrame14(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //GPS Function
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Value 1");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "Value 2");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "value 3");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 41 + y, "value 4");

  mydisplay->display();
}

void Beeldscherm::drawFrame15(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //parameters function
  mydisplay->clear();
  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Parameter 1");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "Parameter 2");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "Parameter 3");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 41 + y, "Parameter 4");
  
  mydisplay->display();
}

void Beeldscherm::drawFrame16(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Error promt
  mydisplay->clear();

  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 26 + y, "ERROR! not able to join");


  mydisplay->display();
}

void Beeldscherm::drawFrame17(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //LoRaWan Logo 
  mydisplay->clear();

  mydisplay->drawXbm(x + 0, y + 0, Lora_Logo_width, Lora_Logo_height, Lora_Logo_bits);

  mydisplay->display();
}

void Beeldscherm::drawFrame18(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Menu single capture
  mydisplay->clear();

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Single capture <--");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "multi capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "GPS");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 41 + y, "Parameters");


  mydisplay->display();
}

void Beeldscherm::drawFrame19(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Menu multi capture
  mydisplay->clear();
  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Single capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "multi capture  <--");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "GPS");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 41 + y, "Parameters");

  mydisplay->display();
}

void Beeldscherm::drawFrame20(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Menu GPS
  mydisplay->clear();

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Single capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "multi capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "GPS            <--");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 41 + y, "Parameters");

  mydisplay->display();
}

void Beeldscherm::drawFrame21(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //Menu parameter
  mydisplay->clear();

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 11 + y, "Single capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 21 + y, "multi capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 31 + y, "GPS");

  mydisplay->setTextAlignment(TEXT_ALIGN_LEFT);
  mydisplay->drawString(0 + x, 41 + y, "Parameters     <--");

  mydisplay->display();
}

void Beeldscherm::drawFrame22(ScreenDisplay *mydisplay, int16_t x, int16_t y) {
  //single capture ACK
  mydisplay->clear();

  msOverlay(mydisplay);

  mydisplay->setFont(ArialMT_Plain_10);

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 21 + y, "Single capture");

  mydisplay->setTextAlignment(TEXT_ALIGN_CENTER);
  mydisplay->drawString(64 + x, 31 + y, "Press A to continue");

  mydisplay->display();
}

void VextON(void)
{
  pinMode(Vext,OUTPUT);
  digitalWrite(Vext, LOW);
}

void VextOFF(void) //Vext default OFF
{
  pinMode(Vext,OUTPUT);
  digitalWrite(Vext, HIGH);
}

void Beeldscherm::GUIInit()
{
  VextON();
  delay(100);
	// The ESP is capable of rendering 60fps in 80Mhz mode
	// but that won't give you much time for anything else
	// run it in 160Mhz mode or just set it to 30 fps
  ui.setTargetFPS(60);

	// Customize the active and inactive symbol
  ui.setActiveSymbol(activeSymbol);
  ui.setInactiveSymbol(inactiveSymbol);

  // You can change this to
  // TOP, LEFT, BOTTOM, RIGHT
  ui.setIndicatorPosition(BOTTOM);

  // Defines where the first frame is located in the bar.
  ui.setIndicatorDirection(LEFT_RIGHT);

  // You can change the transition that is used
  // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_UP, SLIDE_DOWN
  ui.setFrameAnimation(SLIDE_LEFT);

  // Initialising the UI will init the mydisplay too.
  ui.init();
}

// Functions to call side the main
void Beeldscherm::frame1(){
  Beeldscherm::drawFrame1(&mydisplay, 0, 0);}
void Beeldscherm::frame2(){
  Beeldscherm::drawFrame2(&mydisplay, 0, 0);}
void Beeldscherm::frame3(){
  Beeldscherm::drawFrame3(&mydisplay, 0, 0);}
void Beeldscherm::frame4(){
  Beeldscherm::drawFrame4(&mydisplay, 0, 0);}
void Beeldscherm::frame5(){
  Beeldscherm::drawFrame5(&mydisplay, 0, 0);}
void Beeldscherm::frame6(){
  Beeldscherm::drawFrame6(&mydisplay, 0, 0);}
void Beeldscherm::frame7(){
  Beeldscherm::drawFrame7(&mydisplay, 0, 0);}
void Beeldscherm::frame8(){
  Beeldscherm::drawFrame8(&mydisplay, 0, 0);}
void Beeldscherm::frame9(){
  Beeldscherm::drawFrame9(&mydisplay, 0, 0);}
void Beeldscherm::frame10(){
  Beeldscherm::drawFrame10(&mydisplay, 0, 0);}
void Beeldscherm::frame11(){
  Beeldscherm::drawFrame11(&mydisplay, 0, 0);}
void Beeldscherm::frame12(){
  Beeldscherm::drawFrame12(&mydisplay, 0, 0);}
void Beeldscherm::frame13(){
  Beeldscherm::drawFrame13(&mydisplay, 0, 0);}
void Beeldscherm::frame14(){
  Beeldscherm::drawFrame14(&mydisplay, 0, 0);}
void Beeldscherm::frame15(){
  Beeldscherm::drawFrame15(&mydisplay, 0, 0);}
void Beeldscherm::frame16(){
  Beeldscherm::drawFrame16(&mydisplay, 0, 0);}
void Beeldscherm::frame17(){
  Beeldscherm::drawFrame17(&mydisplay, 0, 0);}
void Beeldscherm::frame18(){
  Beeldscherm::drawFrame18(&mydisplay, 0, 0);}
void Beeldscherm::frame19(){
  Beeldscherm::drawFrame19(&mydisplay, 0, 0);}
void Beeldscherm::frame20(){
  Beeldscherm::drawFrame20(&mydisplay, 0, 0);}
void Beeldscherm::frame21(){
  Beeldscherm::drawFrame21(&mydisplay, 0, 0);}
void Beeldscherm::frame22(){
  Beeldscherm::drawFrame2(&mydisplay, 0, 0);}
