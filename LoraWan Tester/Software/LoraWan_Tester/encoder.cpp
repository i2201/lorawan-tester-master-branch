/// \file encoder.cpp
/// \brief Encoder class implementation for lorawan tester
/// \date 28-3-2022
/// \author Marijn Aarts
/// \version 1.0

#include "include/encoder.hpp"

encoder::encoder()
{
    reset();
}


void encoder::reset(void)
{
    for(int i =0; i < SEND_BUFFER_LENGTH; i++)
    {
        send_buffer_battery[i] = 0x00;
    }

    for(int i =0; i < SEND_BUFFER_LENGTH-1; i++)
    {
        send_buffer_no_battery[i] = 0x00;
    }
}


void encoder::add_gps_data(Air530ZClass gps_input)
{
    add_altitude(gps_input.altitude.meters());
    add_longitude(gps_input.location.lng());
    add_latitude(gps_input.location.lat());
    add_accuracy(gps_input.hdop.hdop());
}

void encoder::add_longitude(const double longitude_input)
{
    double temp_longitude = longitude_input;

    //If the gps is out of our specified range we default to 3.0000
    if((temp_longitude < 3) || (temp_longitude > 8))
        temp_longitude = 3.0000;

    temp_longitude *= 10000;
    temp_longitude -= 30000;

    // Convert double to int to remove decimals, convert to char to store in the array
    send_buffer_battery[3] = (char)((int)temp_longitude >> 8);
    send_buffer_battery[4] = (char)(int)temp_longitude;
}

void encoder::add_latitude(const double latitude_input)
{
    double temp_latitude = latitude_input;

    //If the gps is out of our specified range we default to 50.0000
    if((temp_latitude < 50) || (temp_latitude > 54))
        temp_latitude = 50.0000;

    temp_latitude *= 10000;
    temp_latitude -= 500000;



    // Convert double to int to remove decimals, convert to char to store in the array
    send_buffer_battery[1] = (char)((int)temp_latitude >> 8);
    send_buffer_battery[2] = (char)(int)temp_latitude;
}

void encoder::add_altitude(const double altitude_input)
{
    int temp_altitude = altitude_input;

    //If the altitude is out of our specified range we default to 0 meters
    if(temp_altitude > 323 || temp_altitude < -7)
        temp_altitude = 0;

    //+7 to make sure we get a value >= 0
    uint16_t unsigned_temp_altitude = temp_altitude + 7;

    send_buffer_battery[5] = (char)(unsigned_temp_altitude >> 4);
    send_buffer_battery[6] |= (char)(unsigned_temp_altitude << 4);
}

void encoder::add_accuracy(const double accuracy_input)
{
// TODO: Make function correct
    int temp_accuracy = accuracy_input;
    if((accuracy_input > 255) || (accuracy_input < 0))
        temp_accuracy = 0;

    send_buffer_battery[0] = (char)(uint8_t)temp_accuracy;
}

void encoder::add_battery_data(const float battery_level)
{
    uint16_t temp_battery = battery_level * 100;

    //If the battery is out of our specified range we default to 0 volt
    if(temp_battery > 400 || temp_battery < 300)
        temp_battery = 0;

    send_buffer_battery[6] |= (char)(temp_battery >> 8) | BATTERY_DATA_PRESENT_FLAG;
    send_buffer_battery[7] = (char)temp_battery;
}

char *encoder::get_send_bufferptr(int *buffer_size)
{
    if(send_buffer_battery[6] & BATTERY_DATA_PRESENT_FLAG)
    {
        *buffer_size = SEND_BUFFER_LENGTH;
        return send_buffer_battery;
    }
    else
    {
        *buffer_size = SEND_BUFFER_LENGTH - 1;
        for(int i =0; i<SEND_BUFFER_LENGTH - 1; i++)
        {
            send_buffer_no_battery[i] = send_buffer_battery[i];
        }
        return send_buffer_no_battery;
    }
}
