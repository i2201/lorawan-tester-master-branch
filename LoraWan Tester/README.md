# LoRaWAN Test Device
 ***LoRaWAN tester using a LoRaWAN node with a GPS unit built on Heltec Cube Cell HTCC-AB02S***

## Overview
This repository contains the code and documentation regarding the LoRaWAN Tester project as well as a installation and user guide. For the code documentation, please check out the doxygen.

All sections of this README are listed bellow:
- [Use case](#use-case)
- [Principles](#principles)
- [Hardware](#hardware)
- [Installation Guide](#installation-guide)
- [User Guide](#user-guide)

## Use case
Developing the LoraWan tester is part of the assignments in the IOT class 
(e-ELT-6PLG-ESE S6 Smart Embedded Systems (SES) - Internet of Things (IoT), 
HAN University of Applied Science).

The aim of the assignment is to "catch" as much as possible Hexagons in the 
Helium network by participating in the Cargo Mappers initiative. 

## Principles
LoraWan Tester constantly sends out the GPS location encoded every 20s. If a Helium 
Hotspot ‘hears’ that signal, data is passed through the internet and recorded in the 
Mappers database. The Mappers database uses the GPS information from the Mapper to 
‘light up’ the hex on the map.Each 'Light-up' Hexagons contributes the information of
coverage to the map.


## Hardware
Heltec Cube Cell HTCC-AB02S

The CubeCell (TM) are boards are based on the ASR605x (ASR6501, ASR6502), those chips are 
already integrated with the PSoC® 4000 series MCU (ARM® Cortex® M0+ Core) and SX1262 LoRa 
radio. The HTCC-AB02S is a development board with an integrated AIR530Z GPS module that is 
supported by the Arduino framework. See: [**https://heltec.org/project/htcc-ab02s/**](https://heltec.org/project/htcc-ab02s/)

![The CubeCell image](Images/HTCCAB02S.png)

## Installation Guide
In this section is described how to get the LoRaWAN tester up and running. This includes installing the libraries and setting up the device on [Helium](https://www.helium.com). For the hardware setup please look into the hardware section of this README.

### <em><strong>Step 1 - Preparations</strong></em>
Before we start the installation we first need two things.
- [Arduino IDE](https://www.arduino.cc/en/software)
- [Helium console account](https://console.helium.com)

### ***Step 2 - Installing the Firmware***
First, open the Arduino IDE and click `File` -> `Preferences` -> `Settings`
![First install guide image](Images/img1_installguide.png)

![Second install guide image](Images/img2_installguide.png)
Input the following json url to board manager URLs:
```
https://github.com/HelTecAutomation/CubeCell-Arduino/releases/download/V1.4.0/package_CubeCell_index.json
```
![Third install guide image](Images/img3_installguide.png)

Click `Tools` -> `Board:` -> `Boards Manager...`, search Heltec cubecell in the new pop-up dialog, select the latest releases and click install.

![Fourth install guide image](Images/img4_installguide.png)

![Fifth install guide image](Images/img5_installguide.png)

### ***Step 3 - Adding the device to Helium***
First off login to [Console Helium](https://console.helium.com). Then go to the `Devices` tab on the left side of the screen. And click on the button that says `Add New Device`.

![Sixth install guide image](Images/img6_installguide.png)

Give your device a proper name and then press `Save Device`.
![Seventh install guide image](Images/img7_installguide.png)

### ***Step 4 - Adding the Helium device keys to the code***
If you haven't already, download the code from the git repository and open up the `.ino` file. As you can see at the start of the file there are three variables we need to change: devEui, appEui and appKey. Now go to your device on Console Helium and copy the keys to the sketch.

Here is an example of how to convert the key:
```
94A5A0704899D589
```
Before every two characters add `0x` and after a comma.
```
uint8_t devEui[] = { 0x94, 0xA5, 0xA0, 0x70, 0x48, 0x99, 0xD5, 0x89 };
```

Do the same for the devEui, appEui and appKey.

### ***Step 5 - Making small changes to the libraries***
Open up the windows menu, search for `%localappdata%` and press enter. Navigate to `Arduino` -> `packages` -> `CubeCell` -> `hardware` -> `1.4.0` -> `libraries` -> `LoRa` and open the file named `LoRaWan_APP.cpp`. Now download the `LoRaWan_APP.cpp` file from the gitlab and replace the contents of the local file with that of the file from the repository. Now save and close the file.

### ***Step 6 - Setting up the Decoder Function***
In Console Helium go to the `Functions` tab on the left side of the screen. Next click on the button that says `Add New Function`.

![Eighth install guide image](Images/img8_installguide.png)

Next give the function a proper name, select `Function Type` *Decoder* and `Format` *Custom Script*.
![Ninth install guide image](Images/img9_installguide.png)

First remove the old Decoder function.
![Tenth install guide image](Images/img10_installguide.png)

Then copy the new decoder function bellow, paste the new function in and press `Save Function`.

```
function Decoder(bytes, port, uplink_info) {
/*
  The uplink_info variable is an OPTIONAL third parameter that provides the following:

  uplink_info = {
    type: "join",
    uuid: <UUIDv4>,
    id: <device id>,
    name: <device name>,
    dev_eui: <dev_eui>,
    app_eui: <app_eui>,
    metadata: {...},
    fcnt: <integer>,
    reported_at: <timestamp>,
    port: <integer>,
    devaddr: <devaddr>,
    hotspots: {...},
    hold_time: <integer>
  }
*/

  if (uplink_info) {
    // do something with uplink_info fields
  }
  
  let decoded;
  
  decoded={
  accuracy: bytes[0],
  latitude: (((bytes[1] << 8) | (bytes[2])) + 500000) / 10000, 
  longitude: (((bytes[3] << 8) | (bytes[4])) + 30000) / 10000,
  altitude: ((bytes[5] << 4) | (bytes[6] >> 4)) - 7, 
  battery: ( ( (bytes[6]  & 0b00000111) << 8) | bytes[7]) / 100
  }
  

  return decoded;
}

```

### ***Step 7 - Adding integrations***

The integrations used in this project are: [Helium Cargo](https://cargo.helium.com), [TTN Mapper](https://ttnmapper.org/heatmap/), [Helium Mappers](https://mappers.helium.com) and a custom HTTP integration for the Hex catching contest. For a guide on how to add integrations to Console Helium you can use these guides:
- [Helium Integrations General Documentation](https://docs.helium.com/use-the-network/console/integrations/)
- [Helium Cargo Integration](https://docs.helium.com/use-the-network/console/integrations/cargo/)
- [Helium Mappers](https://docs.helium.com/use-the-network/coverage-mapping/mappers-quickstart/)
- [TTN Mapper Integration](https://docs.ttnmapper.org/integration/helium.html)

When following these guides you don't need to worry about the payload format since the decoder function already does that for you.

### ***Step 8 - Setting up the Flow***
Go to the `Flows` tab on the left side of the screen.

Click the plus symbol next to `Nodes` on the top left of the screen.
![Eleventh install guide image](Images/img11_installguide.png)

Go to the `Devices` tab and drag the device bellow onto the screen. Do the same for the `Functions` tab as well as all the `Integrations`.

![Twelfth install guide image](Images/img12_installguide.png)

Once all components are on the screen start connecting them into a similar order as in the image bellow. The order must be `Device` -> `Function` -> `Integration`.

![Thirteenth install guide image](Images/Flows.png)

## User Guide 
![drawing](Images/GUI-state-diagram-1.png)

### ***Step 1 - Choose the function***
After the user power up the device, the screen will show the menu. Then, the user can use the `Button Up` and `Button Down` to choose one of these functions :
- Menu_Single_Capture
- Menu_Multi_Capture
- Menu_Parameters
- Menu_GPS


### ***Step 2 - Run the function***
According to the chosen option in step 1, the response from the device will be different.
1. Menu_Single_Capture
>Press the `Button A` to go to the next state __Single_Capture_Ack__.

>Press the `Button A` once more time to run the __Single_Capture_Function__. 
 Press the `Button B` to go back.

>After seeing the result, pressing the `Button A` again will return back to the main menu.

2. Menu_Multi_Capture
>Press the `Button A` to go to the next state __Multi_Capture_Set_value__ to set the value. Then, press the
`Button Up` or `Button Down` to move among the value_1, value_2, value_3 and value_4.

>Press the `Button A` once more time to run the __Multi_Capture_Function__. 

>After seeing the result, pressing the `Button A` again will return back to the main menu.

3. Menu_Parameters
> Press the `Button A` to show the parameters. Then the user can go back by pressing the `Button B`.

4. Menu_GPS

>Press the `Button A` to run the GPS_Function. The device will search for the closest GPS and display the parameters on the screen. Then the user can go back by pressing the `Button B`.