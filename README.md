# LoRaWAN Test Device
 ***LoRaWAN tester using a LoRaWAN node with a GPS unit built on Heltec Cube Cell HTCC-AB02S***
 
## Overview
This repository contains the code and documentation regarding the LoRaWAN Tester project as well as a installation and user guide. For the code documentation, please check out the doxygen.

The repository consists of two projects. "LoRaWAN Tester" is the finished project, "LoRaWAN Tester - statemachine" is a work-in-progress project where a statemachine is being implemented.
