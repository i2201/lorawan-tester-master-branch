@startuml
[*] --> init
init --> Menu_Single_Capture

Menu_Single_Capture -> Menu_Multi_Capture : E_Pressed_Down
Menu_Single_Capture -> Menu_GPS : E_Press_Up
Menu_Single_Capture : Point to Single Capture option in menu
Menu_Multi_Capture -> Menu_Parameters : E_Pressed_Down
Menu_Multi_Capture -> Menu_Single_Capture : E_Pressed_Up
Menu_Multi_Capture : Point to Multi Capture option in menu
Menu_Parameters -> Menu_GPS : E_Pressed_Down
Menu_Parameters -> Menu_Multi_Capture : E_Pressed_Up
Menu_Parameters : Point to Parameter option in menu
Menu_GPS -> Menu_Single_Capture : E_Pressed_Down
Menu_GPS -> Menu_Parameters : E_Pressed_Up
Menu_GPS: Point to GPS option in menu

Menu_Single_Capture --> Single_Capture_Ack : E_Pressed_A_Button
Single_Capture_Ack --> Single_Capture_Function : E_Pressed_A_Button
Single_Capture_Ack --> Menu_Single_Capture : E_Pressed_B_Button
Single_Capture_Ack : Asks to confirm option
Single_Capture_Function --> Menu_Single_Capture

Menu_Multi_Capture --> Multi_Capture_Set_value_1 : E_Pressed_A_Button
Multi_Capture_Set_value_1 -> Multi_Capture_Set_value_4 : E_Pressed_Up
Multi_Capture_Set_value_1 -> Multi_Capture_Set_value_2 : E_Pressed_Down
Multi_Capture_Set_value_1 : Point to set value 1 in menu
Multi_Capture_Set_value_1 --> Multi_Capture_function_1 : E_Pressed_A_Button
Multi_Capture_Set_value_1 --> Menu_Multi_Capture : E_Pressed_B_Button
Multi_Capture_function_1 --> Menu_Multi_Capture 
Multi_Capture_function_1 : Makes [set amount 1] captures

Multi_Capture_Set_value_2 -> Multi_Capture_Set_value_1 : E_Pressed_Up
Multi_Capture_Set_value_2 -> Multi_Capture_Set_value_3 : E_Pressed_Down
Multi_Capture_Set_value_2 : Point to set value 2 in menu
Multi_Capture_Set_value_2 --> Multi_Capture_function_2 : E_Pressed_A_Button
Multi_Capture_Set_value_2 --> Menu_Multi_Capture : E_Pressed_B_Button
Multi_Capture_function_2 --> Menu_Multi_Capture 
Multi_Capture_function_2 : Makes [set amount 2] captures

Multi_Capture_Set_value_3 -> Multi_Capture_Set_value_2 : E_Pressed_Up
Multi_Capture_Set_value_3 -> Multi_Capture_Set_value_4 : E_Pressed_Down
Multi_Capture_Set_value_3 : Point to set value 3 in menu
Multi_Capture_Set_value_3 --> Multi_Capture_function_3 : E_Pressed_A_Button
Multi_Capture_Set_value_3 --> Menu_Multi_Capture : E_Pressed_B_Button
Multi_Capture_function_3 --> Menu_Multi_Capture 
Multi_Capture_function_3 : Makes [set amount 3] captures

Multi_Capture_Set_value_4 -> Multi_Capture_Set_value_3 : E_Pressed_Up
Multi_Capture_Set_value_4 -> Multi_Capture_Set_value_1 : E_Pressed_Down
Multi_Capture_Set_value_4 : Point to set value 4 in menu
Multi_Capture_Set_value_4 --> Multi_Capture_function_4 : E_Pressed_A_Button
Multi_Capture_Set_value_4 --> Menu_Multi_Capture : E_Pressed_B_Button
Multi_Capture_function_4 --> Menu_Multi_Capture 
Multi_Capture_function_4 : Makes [set amount 4] captures

Menu_Parameters --> Parameter_Function : E_Pressed_A_Button
Parameter_Function --> Menu_Parameters : E_Pressed_B_Button
Parameter_Function : Shows the parameters

Menu_GPS --> GPS_Function : E_Pressed_A_Button
GPS_Function --> Menu_GPS : E_Pressed_Down
GPS_Function : Searches for the nearest GPS and Displays the parameters

@enduml