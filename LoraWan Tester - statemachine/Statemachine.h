#ifndef STATEMACHINE_H
#define STATEMACHINE_H

typedef int error_t;

typedef enum {NOERR} error_e;

typedef enum {S_NO, S_SEQ, S_INIT, S_MENU_SINGLE_CAPTURE, S_MENU_MULTI_CAPTURE, S_MENU_PARAMETERS, S_MENU_GPS,
              S_SINGLE_CAPTURE_ACK, S_SINGLE_CAPTURE_FUNCTION, S_CAPTURE_DONE_0,
              S_MULTI_CAPTURE_VALUE_1, S_MULTI_CAPTURE_VALUE_2, S_MULTI_CAPTURE_VALUE_3, S_MULTI_CAPTURE_VALUE_4, 
              S_MULTI_CAPTURE_FUNCTION_1, S_MULTI_CAPTURE_FUNCTION_2, S_MULTI_CAPTURE_FUNCTION_3, S_MULTI_CAPTURE_FUNCTION_4, S_CAPTURE_DONE_1,
              S_PARAMETERS_FUNCTION, S_GPS_FUNCTION }state_e;

typedef enum {E_NO, E_SEQ, E_INIT, E_PRESSED_UP_BUTTON, E_PRESSED_DOWN_BUTTON, E_PRESSED_A_BUTTON, E_PRESSED_B_BUTTON }event_e;

class MainWindow;

class StateMachine {
public:

   StateMachine():
      currentState(S_INIT){}
   ~StateMachine() {}

   void handleEvent(event_e eventIn);
   state_e getCurrentState() const { return currentState; }


   state_e currentState;
   event_e statemachine(event_e eventIn);

};


#endif