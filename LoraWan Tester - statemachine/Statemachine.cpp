#include "statemachine.h"

void StateMachine::handleEvent(event_e eventIn)
{
  while(eventIn ! = E_NO)
  {
    eventIn = statemachine(eventIn);
  }
}

event_e StatMachine::statemachine(event_e eventIn)
{
  state_e nextState = S_NO
  event_e eventOut = E_NO

  switch(currentState)
  {
    case S_INIT:
      nextState = S_MENU_SINGLE_CAPTURE;
      eventOut = E_NO;
      break;

    case S_MENU_SINGLE_CAPTURE:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MENU_GPS;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_SINGLE_CAPTURE_ACK;
        break;
      default:
        // print ERROR MENU (single capture)
        break;
      eventOut = E_NO;
      break;

    case S_MENU_MULTI_CAPTURE:
      case E_PRESSED_UP_BUTTON:
        NextState = S_MENU_SINGLE_CAPTURE;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MENU_PARAMETERS;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_MULTI_CAPTURE_VALUE_1
        break;
      default:
        // print ERROR MENU (multi capture)
        break;
      eventOut = E_NO
      break;

    case S_MENU_PARAMETERS:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MENU_GPS;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_PARAMETERS_FUNCTION;
        break;
      default:
        // print ERROR MENU (Parameters)
        break;
      eventOut = E_NO;
      break;

    case S_MENU_GPS:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MENU_PARAMETERS
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MENU_SINGLE_CAPTURE;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_GPS_FUNCTION;
        break;
      default:
        // print ERROR MENU (GPS)
        break;
      eventOut = E_NO;
      break;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

    case S_SINGLE_CAPTURE_ACK:
      case E_PRESSED_A_BUTTON:
        nextState = S_SINGLE_CAPTURE_FUNCTION;
        eventOut = E_SEQ;
        break;
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_SINGLE_CAPTURE
        eventOut = E_NO;
        break;
      default:
        //print ERROR ACK (single)
        break;
      break;
      
    case S_SINGLE_CAPTURE_FUNCTION:
      nextSate = S_CAPTURE_DONE_0;
      eventOut = E_NO;
      break;

    case S_CAPTURE_DONE_0:
      case E_PRESSED_A_BUTTON:
      nextSate = S_CAPTURE_DONE_0;
      eventOut = E_NO;
      default:
        //print ERROR (capture)
      break;

    /*case S_CAPTURE_ERROR_0:
      break;*/
      
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

    case S_MULTI_CAPTURE_VALUE_1:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MULTI_CAPTURE_VALUE_4;
        eventOut = E_NO;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_2;
        eventOut = E_NO;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_MULTI_CAPTURE_FUNCTION_1;
        eventOut = E_SEQ;
        break;
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        eventOut = E_NO;
        break;
      break;

    case S_MULTI_CAPTURE_VALUE_2:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_1;
        eventOut = E_NO;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_3;
        eventOut = E_NO;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_MULIT_CAPTURE_FUNCTION_2;
        eventOut = E_SEQ;
        break;
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        eventOut = E_NO;
        break;
      break;

    case S_MULTI_CAPTURE_VALUE_3:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_2;
        eventOut = E_NO;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_4;
        eventOut = E_NO;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_MULIT_FUNCTION_VALUE_3;
        eventOut = E_SEQ;
        break;
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        eventOut = E_NO;
        break;
      break;

    case S_MULTI_CAPTURE_VALUE_4:
      case E_PRESSED_UP_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_3;
        eventOut = E_NO;
        break;
      case E_PRESSED_DOWN_BUTTON:
        nextState = S_MULIT_CAPTURE_VALUE_1;
        eventOut = E_NO;
        break;
      case E_PRESSED_A_BUTTON:
        nextState = S_MULTI_FUNCTION_VALUE_4;
        eventOut = E_SEQ;
        break;
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        eventOut = E_NO;
        break;
      break;

    case S_MULTI_CAPTURE_FUNCTION_1:
      nextState = S_CAPTURE_DONE_1;
      eventOut = E_NO;
      break;

    case S_MULTI_CAPTURE_FUNCTION_2:
      nextState = S_CAPTURE_DONE_1;
      eventOut = E_NO;
      break;

    case S_MULTI_CAPTURE_FUNCTION_3:
      nextState = S_CAPTURE_DONE_1;
      eventOut = E_NO;
      break;

    case S_MULTI_CAPTURE_FUNCTION_4:
      nextState = S_CAPTURE_DONE_1;
      eventOut = E_NO;
      break;

    case S_CAPTURE_DONE_1:
      case E_PRESSED_A_BUTTON:
        nextState = S_MENU_MULTI_CAPTURE;
        break;
      eventOut = E_NO;
      break;
       
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
    /*case S_CAPTURE_ERROR_1:
      case E_PRESSED_A_BUTTON:
        break;
      break;*/

    case S_PARAMETERS_FUNCTION:
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_PARAMETERS;
        break;
      eventOut = E_NO;
      break;
      
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

    case S_GPS_FUNCTION:
      case E_PRESSED_B_BUTTON:
        nextState = S_MENU_GPS;
        break;
      eventOut = E_NO;
      break;

    default: 
      // Print ERROR
      break;
  }

    currentState = nextState;
    return eventOut;
}